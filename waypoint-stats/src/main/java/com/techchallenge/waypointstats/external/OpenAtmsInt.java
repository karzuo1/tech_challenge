package com.techchallenge.waypointstats.external;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.techchallenge.waypointstats.constants.IConfiguration;
import com.techchallenge.waypointstats.pojo.Airport;
import com.techchallenge.waypointstats.pojo.AirportSid;
import com.techchallenge.waypointstats.pojo.AirportStar;
import com.techchallenge.waypointstats.pojo.SID;
import com.techchallenge.waypointstats.pojo.STAR;


/**
 * This class interface with the Open ATMS Rest API and makes all
 * necessary functionality calls to the API that this module requires.
 *
 */
public class OpenAtmsInt {
	private WebClient webClient;
	
	
	public OpenAtmsInt() {
		webClient = WebClient.builder()
	        .baseUrl(IConfiguration.BASE_URL)
	        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
	        .defaultHeader(IConfiguration.KEY_STR, IConfiguration.API_KEY)
	        .build();
	}
	
	/**
	 * Get the list of airports
	 * @return List<Airport> List of airports
	 */
	public List<Airport> getAirports() {
		 return webClient.get()
	        .uri("/airac/airports")
	        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
	        .retrieve()
	        .bodyToFlux(Airport.class)
	        .collectList().block();
	}
	
	/**
	 * Get the list of SIDs given the ICAO
	 * @param icao (String): ICAO of an airport
	 * @return List<SID>: List of SIDs of an airport with given ICAO
	 */
	public List<SID> getSids(String icao) {
		 return webClient.get()
	        .uri("/airac/sids/airport/" + icao)
	        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
	        .retrieve()
	        .bodyToFlux(SID.class)
	        .collectList().block();
	}
	
	/**
	 * Get the list of STARs given the ICAO
	 * @param icao (String): ICAO of an airport
	 * @return List<STAR>: List of STARs of an airport with given ICAO
	 */
	public List<STAR> getStars(String icao) {
		 return webClient.get()
	        .uri("/airac/stars/airport/" + icao)
	        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
	        .retrieve()
	        .bodyToFlux(STAR.class)
	        .collectList().block();
	}
	
	/**
	 * This function call the Open ATMs API to get a list of
	 * airports with its associated SIDs
	 * @return AirportSid[]
	 */
	public AirportSid[] getAirportSid() {
		List<Airport> airports = getAirports();
		ArrayList<AirportSid> airSidList = new ArrayList<AirportSid>();
		
		//for each airport, compute the results
		Iterator<Airport> it = airports.iterator();
		while(it.hasNext()) {
			List<SID> sid; //List of SIDs for current airport
			
			//Iterate through list of airport
			Airport curr = it.next();
			
			//Get the list of SIDs for current airport
			sid = getSids(curr.getIcao());
			
			//Store into msg list
			airSidList.add(new AirportSid(curr, sid.toArray(new SID[0])));
		}
		
		return airSidList.toArray(new AirportSid[0]);
	}
	
	/**
	 * This function call the Open ATMs API to get a list of
	 * airports with its associated STARs
	 * @return AirportStar[]
	 */
	public AirportStar[] getAirportStar() {
		List<Airport> airports = getAirports();
		ArrayList<AirportStar> airStarList = new ArrayList<AirportStar>();
		
		//for each airport, compute the results
		Iterator<Airport> it = airports.iterator();
		while(it.hasNext()) {
			List<STAR> star; //List of STARs for current airport
			
			//Iterate through list of airport
			Airport curr = it.next();
			
			//Get the list of SIDs for current airport
			star = getStars(curr.getIcao());
			
			//Store into msg list
			airStarList.add(new AirportStar(curr, star.toArray(new STAR[0])));
		}
			
		return airStarList.toArray(new AirportStar[0]);
	}
}
