package com.techchallenge.waypointstats.datastore;

import java.util.logging.Logger;

import com.techchallenge.waypointstats.constants.IConfiguration;
import com.techchallenge.waypointstats.constants.IKafkaConstants;
import com.techchallenge.waypointstats.kafka.AirportDataConsumer;
import com.techchallenge.waypointstats.pojo.AirportSid;
import com.techchallenge.waypointstats.pojo.AirportStar;

/**
 * This class starts Kafka consumer to poll the topics for airport to SID or STAR assoc
 * It then store the latest value in memory for quick retrieval on request
 */
public class RouteStore {
	private final static Logger LOGGER = 
			Logger.getLogger(TopWaypointsStore.class.getName());
	
	// static variable single_instance of type Singleton
    private static RouteStore singleInstance = null;
    
    //Data to store
    private AirportSid airSid[];
    private AirportStar airStar[];
	
	//Private constructor (Singleton Pattern)
	private RouteStore() {
		//Start consumer to kafka
		AirportDataConsumer sidConsumer = new AirportDataConsumer(
				IKafkaConstants.TOPIC_AIR_SID, IConfiguration.POLL_DELAY);
		AirportDataConsumer starConsumer = new AirportDataConsumer(
				IKafkaConstants.TOPIC_AIR_STAR, IConfiguration.POLL_DELAY);
		Thread sidThread = new Thread(sidConsumer);
		Thread starThread = new Thread(starConsumer);
		sidThread.start();
		starThread.start();
		LOGGER.info("Top waypoints datastore ready.");
	}
	
	public static RouteStore getInstance() {
		if (singleInstance == null)
            singleInstance = new RouteStore();
  
        return singleInstance;
	}
	
	public AirportSid[] getAirportSid() {
		return airSid;
	}
	
	public AirportStar[] getAirportStar() {
		return airStar;
	}
	
	public void setAirportSid(AirportSid airSid[]) {
		this.airSid = airSid;
	}
	
	public void setAirportStar(AirportStar airStar[]) {
		this.airStar = airStar;
	}
}