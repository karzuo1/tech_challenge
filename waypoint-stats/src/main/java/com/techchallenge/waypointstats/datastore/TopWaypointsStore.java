package com.techchallenge.waypointstats.datastore;

import java.util.logging.Logger;

import com.techchallenge.waypointstats.constants.IConfiguration;
import com.techchallenge.waypointstats.constants.IKafkaConstants;
import com.techchallenge.waypointstats.kafka.StatsDataConsumer;
import com.techchallenge.waypointstats.messages.TopWaypointMsg;

/**
 * This class starts Kafka consumer to poll the Top Waypoints topics for SID or STAR
 * It then store the latest value in memory for quick retrieval on request
 */
public class TopWaypointsStore {
	private final static Logger LOGGER = 
			Logger.getLogger(TopWaypointsStore.class.getName());
	
	// static variable single_instance of type Singleton
    private static TopWaypointsStore singleInstance = null;
    
    //Data to store
    private TopWaypointMsg topWaypointSid[];
    private TopWaypointMsg topWaypointStar[];
	
	//Private constructor (Singleton Pattern)
	private TopWaypointsStore() {
		//Start consumer to kafka
		StatsDataConsumer sidConsumer = new StatsDataConsumer(
				IKafkaConstants.TOPIC_SID, IConfiguration.POLL_DELAY);
		StatsDataConsumer starConsumer = new StatsDataConsumer(
				IKafkaConstants.TOPIC_STAR, IConfiguration.POLL_DELAY);
		Thread sidThread = new Thread(sidConsumer);
		Thread starThread = new Thread(starConsumer);
		sidThread.start();
		starThread.start();
		LOGGER.info("Top waypoints datastore ready.");
	}
	
	public static TopWaypointsStore getInstance() {
		if (singleInstance == null)
            singleInstance = new TopWaypointsStore();
  
        return singleInstance;
	}
	
	public TopWaypointMsg[] getTopWaypointSid() {
		while(topWaypointSid == null); //wait for results
		return topWaypointSid;
	}
	
	public TopWaypointMsg[] getTopWaypointStar() {
		while(topWaypointStar == null); //wait for results
		return topWaypointStar;
	}
	
	public void setTopwaypointSid(TopWaypointMsg topWaypointSid[]) {
		this.topWaypointSid = topWaypointSid;
	}
	
	public void setTopwaypointStar(TopWaypointMsg topWaypointStar[]) {
		this.topWaypointStar = topWaypointStar;
	}
}
