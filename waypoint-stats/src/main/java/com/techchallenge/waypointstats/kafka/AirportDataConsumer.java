package com.techchallenge.waypointstats.kafka;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techchallenge.waypointstats.constants.IConfiguration;
import com.techchallenge.waypointstats.constants.IKafkaConstants;
import com.techchallenge.waypointstats.datastore.RouteStore;
import com.techchallenge.waypointstats.pojo.AirportSid;
import com.techchallenge.waypointstats.pojo.AirportStar;


/**
 * This class is the Kafka consumer for consuming the last message
 * from the topics of airport data for SID/STARs
 *
 */
public class AirportDataConsumer implements Runnable{
	private final static Logger LOGGER = 
			Logger.getLogger(StatsDataConsumer.class.getName());
	
	private Consumer<Long, String> consumer;
	private String topic;
	private int delay;
	
	public AirportDataConsumer(String topic, int delay) {
		this.topic = topic;
		this.delay = delay;
	}
	
    @Override
    public void run() { 
		consumer = ConsumerCreator.createConsumer(topic, topic); //1 group per topic
		consumer.poll(Duration.ofMillis(delay));

		consumer.assignment().forEach(System.out::println);

        AtomicLong maxTimestamp = new AtomicLong();
        AtomicReference<ConsumerRecord<Long, String>> latestRecord = new AtomicReference<>();

        while(true) {
	        // get the last offsets for each partition
	        consumer.endOffsets(consumer.assignment()).forEach((topicPartition, offset) -> {
	            LOGGER.info("offset: "+offset);
	
	            // seek to the last offset of each partition
	            consumer.seek(topicPartition, (offset==0) ? offset:offset - 1);
	
	            // poll to get the last record in each partition
	            consumer.poll(Duration.ofMillis(delay)).forEach(record -> {
	
	                // the latest record in the 'topic' is the one with the highest timestamp
	                if (record.timestamp() > maxTimestamp.get()) {
	                    maxTimestamp.set(record.timestamp());
	                    latestRecord.set(record);
	                }
	            });
	        });
	        
	        ObjectMapper objectMapper = new ObjectMapper();
			
	        if (topic.compareTo(IKafkaConstants.TOPIC_AIR_SID) == 0) {
		        AirportSid msgList[] = null;
				
				try {
					if (latestRecord.get() != null) {
						msgList = objectMapper.readValue(latestRecord.get().value(), 
								new TypeReference<List<AirportSid>>(){}).toArray(new AirportSid[0]);
					}
				} catch (JsonMappingException e) {
					LOGGER.severe(e.toString());
				} catch (JsonProcessingException e) {
					LOGGER.severe(e.toString());
				}
				RouteStore.getInstance().setAirportSid(msgList);
	        }
			else if (topic.compareTo(IKafkaConstants.TOPIC_AIR_STAR) == 0) {
		        AirportStar msgList[] = null;
				
				try {
					if (latestRecord.get() != null) {
						msgList = objectMapper.readValue(latestRecord.get().value(), 
								new TypeReference<List<AirportStar>>(){}).toArray(new AirportStar[0]);
					}
				} catch (JsonMappingException e) {
					LOGGER.severe(e.toString());
				} catch (JsonProcessingException e) {
					LOGGER.severe(e.toString());
				}
				RouteStore.getInstance().setAirportStar(msgList);
			}
				
	        // commits the offset of record to broker. 
	        consumer.commitAsync();
	        
	        try {
				Thread.sleep(IConfiguration.SLEEP);
			} catch (InterruptedException e) {
				LOGGER.severe(e.toString());
			}
        }
    }
}
