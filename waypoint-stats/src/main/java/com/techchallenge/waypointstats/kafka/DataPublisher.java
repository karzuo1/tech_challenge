package com.techchallenge.waypointstats.kafka;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techchallenge.waypointstats.WaypointStatsProcessor;
import com.techchallenge.waypointstats.constants.IKafkaConstants;
import com.techchallenge.waypointstats.datastore.RouteStore;
import com.techchallenge.waypointstats.external.OpenAtmsInt;
import com.techchallenge.waypointstats.pojo.AirportSid;
import com.techchallenge.waypointstats.pojo.AirportStar;
import com.techchallenge.waypointstats.pojo.SID;
import com.techchallenge.waypointstats.pojo.STAR;

/**
 * This class contains functions that sends message to Kafka
 * 
 */
public class DataPublisher {
	private final static Logger LOGGER = 
			Logger.getLogger(DataPublisher.class.getName());
	
	// static variable single_instance of type Singleton
    //private static DataPublisher singleInstance = null;
	
    private OpenAtmsInt openAtms; //link to external REST API
	private Producer<Long, String> producer;
	private WaypointStatsProcessor wpstats;
	
	//Private constructor (Singleton Pattern)
	public DataPublisher(String clientID) {
		producer = ProducerCreator.createProducer(clientID);
		openAtms = new OpenAtmsInt();
		wpstats = new WaypointStatsProcessor();
	}
	
	//public static DataPublisher getInstance() {
	//	if (singleInstance == null)
    //        singleInstance = new DataPublisher();
  
     //   return singleInstance;
	//}
	
	
	/**
	 * This function polls the Open ATMs API for any changes
	 * and if there is, it will send updated results of Top waypoints for SIDs
	 * to the Kafka topic
	 */
	public void produceSidStats() {		
		ObjectMapper mapper = new ObjectMapper();
		String sidJson;
		AirportSid[] airSids = getNewSids();
		
		if (airSids != null) {
			try {
				//Publish SIDs to topic
				sendAirportSid(airSids);
		
				//Converting the Object to JSONString
				sidJson = mapper.writeValueAsString(wpstats.topWaypointsSid(airSids));
			
				//Publish to topic
				ProducerRecord<Long, String> record = 
						new ProducerRecord<Long, String>
								(IKafkaConstants.TOPIC_SID,	sidJson);
				try {
					RecordMetadata metadata = producer.send(record).get();
					LOGGER.info("Record sent to " + metadata.topic() + " with key" + 
							" to partition " + metadata.partition()
							+ " with offset " + metadata.offset());
				} 
				catch (ExecutionException e) {
					LOGGER.severe("Error in sending record: " + e);
				} 
				catch (InterruptedException e) {
					LOGGER.severe("Error in sending record: " + e);
				}
			}
			catch(Exception error) {
				LOGGER.severe(error.getMessage());
			}
		}
	}
	
	/**
	 * This function polls the Open ATMs API for any changes
	 * and if there is, it will send updated results of Top waypoints for STARs
	 * to the Kafka topic
	 */
	public void produceStarStats() {		
		ObjectMapper mapper = new ObjectMapper();
		String starJson;
		AirportStar[] airStars = getNewStars();
		
		if (airStars != null) {
			try {
				//Publish SIDs to topic
				sendAirportStar(airStars);
				
				//Converting the Object to JSONString
				starJson = mapper.writeValueAsString(wpstats.topWaypointsStar(airStars));
				
				//Publish to topic
				ProducerRecord<Long, String> record = 
						new ProducerRecord<Long, String>
								(IKafkaConstants.TOPIC_STAR, starJson);
				try {
					RecordMetadata metadata = producer.send(record).get();
					LOGGER.info("Record sent to " + metadata.topic() + " with key" + 
							" to partition " + metadata.partition()
							+ " with offset " + metadata.offset());
				} 
				catch (ExecutionException e) {
					LOGGER.severe("Error in sending record: " + e);
				} 
				catch (InterruptedException e) {
					LOGGER.severe("Error in sending record: " + e);
				}
			}
			catch(Exception error) {
				LOGGER.severe(error.getMessage());
			}
		}
	}
	
	
	/**
	 * This function send airport-sid associations info into topic on demand
	 * @param airSids[] (AirportSid): The POJO array containing the data
	 */
	private void sendAirportSid(AirportSid airSids[]) {
		ObjectMapper mapper = new ObjectMapper();
		String sidJson;
		
		try {
			//Converting the Object to JSONString
			sidJson = mapper.writeValueAsString(airSids);
			
			ProducerRecord<Long, String> record = 
					new ProducerRecord<Long, String>
							(IKafkaConstants.TOPIC_AIR_SID,	sidJson);
			try {
				RecordMetadata metadata = producer.send(record).get();
				LOGGER.info("Record sent to " + metadata.topic() + " with key" + 
						" to partition " + metadata.partition()
						+ " with offset " + metadata.offset());
			} 
			catch (ExecutionException e) {
				LOGGER.severe("Error in sending record: " + e);
			} 
			catch (InterruptedException e) {
				LOGGER.severe("Error in sending record: " + e);
			}
		}
		catch(Exception error) {
			LOGGER.severe(error.getMessage());
		}
	}

	/**
	 * This function send airport-sid associations info into topic on demand
	 * @param airSids[] (AirportSid): The POJO array containing the data
	 */
	private void sendAirportStar(AirportStar airStars[]) {
		ObjectMapper mapper = new ObjectMapper();
		String starJson;
		
		try {
			//Converting the Object to JSONString
			starJson = mapper.writeValueAsString(airStars);
			
			ProducerRecord<Long, String> record = 
					new ProducerRecord<Long, String>
							(IKafkaConstants.TOPIC_AIR_STAR, starJson);
			try {
				RecordMetadata metadata = producer.send(record).get();
				LOGGER.info("Record sent to " + metadata.topic() + " with key" + 
						" to partition " + metadata.partition()
						+ " with offset " + metadata.offset());
			} 
			catch (ExecutionException e) {
				LOGGER.severe("Error in sending record: " + e);
			} 
			catch (InterruptedException e) {
				LOGGER.severe("Error in sending record: " + e);
			}
		}
		catch(Exception error) {
			LOGGER.severe(error.getMessage());
		}
	}

	/**
	 * This function will call Open ATMs API to get current SIDs associated 
	 * with each airport data. If data is new (not yet in Kafka or updated), 
	 * return the data. Null otherwise
	 * @return AirportSid[] if data is new or non-existing in kafka. Null otherwise
	 */
	private AirportSid[] getNewSids() {
		AirportSid oldList[] = RouteStore.getInstance().getAirportSid();
		AirportSid newList[] = openAtms.getAirportSid();
		HashMap<String,SID[]> oldHash = new HashMap<String,SID[]>();

		//if nothing in topic
		if (oldList == null) {
			return newList;
		}
	
		if (oldList.length != newList.length) {
			return newList;
		}
		
		//Put old list into hashmap with ICAO as key, SIDs as value
		for (int i=0; i< oldList.length; i++) {
			oldHash.put(oldList[i].getAirport().getIcao(), 
					oldList[i].getSids());
		}
		
		//Compare new list with old list
		for (int i=0; i< newList.length; i++) {
			SID oldSids[] = oldHash.get(
					newList[i].getAirport().getIcao());
			SID newSids[] = newList[i].getSids();
			
			if (oldSids.length != newSids.length) {
				return newList;
			}
			
			HashSet<String> oldSet = new HashSet<String>();
			HashSet<String> newSet = new HashSet<String>();
			
			for (int j=0; j< oldSids.length; j++) {
				oldSet.add(oldSids[i].getName());
				newSet.add(newSids[i].getName());
			}
			if (!oldSet.containsAll(newSet)){
				return newList;
			}
		}
		
		return null; //Same data already exist in kafka
	}
	
	/**
	 * This function will call Open ATMs API to get current STARs associated 
	 * with each airport data. If data is new (not yet in Kafka or updated), 
	 * return the data. Null otherwise
	 * @return AirportStar[] if data is new or non-existing in kafka. Null otherwise
	 */
	private AirportStar[] getNewStars() {
		AirportStar oldList[] = RouteStore.getInstance().getAirportStar();
		AirportStar newList[] = openAtms.getAirportStar();
		HashMap<String,STAR[]> oldHash = new HashMap<String,STAR[]>();
		
		//if nothing in topic
		if (oldList == null) {
			return newList;
		}	

		if (oldList.length != newList.length) {
			return newList;
		}
		
		//Put old list into hashmap with ICAO as key, STARs as value
		for (int i=0; i< oldList.length; i++) {
			oldHash.put(oldList[i].getAirport().getIcao(), 
					oldList[i].getStars());
		}
		
		//Compare new list with old list
		for (int i=0; i< newList.length; i++) {
			STAR oldStars[] = oldHash.get(
					newList[i].getAirport().getIcao());
			STAR newStars[] = newList[i].getStars();
			
			if (oldStars.length != newStars.length) {
				return newList;
			}
			
			HashSet<String> oldSet = new HashSet<String>();
			HashSet<String> newSet = new HashSet<String>();
			
			for (int j=0; j< oldStars.length; j++) {
				oldSet.add(oldStars[i].getName());
				newSet.add(newStars[i].getName());
			}
			if (!oldSet.containsAll(newSet)){
				return newList;
			}
		}

		
		return null; //Same data already exist in kafka
	}
}
