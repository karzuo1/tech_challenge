package com.techchallenge.waypointstats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WaypointStatsApplication {

	public static void main(String[] args) {
		System.setProperty("server.port","8090");
		SpringApplication.run(WaypointStatsApplication.class, args);
	}

}
