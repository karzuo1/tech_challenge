package com.techchallenge.waypointstats.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class defines the message to be returned when the information
 * of a waypoint is requested.
 */
public class TopWaypoint {
	private final String name; //name of the waypoint
	private final int count; //num of SID/STAR associated
	
	public TopWaypoint(@JsonProperty("name") String name, 
			@JsonProperty("count") int count) {
		this.name = name;
		this.count = count;
	}	
	
	public String getName() {
		return name;
	}
	
	public int getCount() {
		return count;
	}
}
