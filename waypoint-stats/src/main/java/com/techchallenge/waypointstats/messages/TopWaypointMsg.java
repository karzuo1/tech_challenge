package com.techchallenge.waypointstats.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class defines the message to be returned when the information
 * of top waypoints of an airport is requested.
 * 
 */
public class TopWaypointMsg {

	private final String airport; //icao of airport
	private final TopWaypoint topWaypoints[];

	public TopWaypointMsg(@JsonProperty("airport") String airport, 
			@JsonProperty("topWaypoints") TopWaypoint topWaypoints[]) {
		this.airport = airport;
		this.topWaypoints = topWaypoints;
	}

	public String getAirport() {
		return airport;
	}

	public TopWaypoint[] getTopWaypoints() {
		return topWaypoints;
	}
	

}
