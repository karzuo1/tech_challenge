package com.techchallenge.waypointstats;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techchallenge.waypointstats.datastore.TopWaypointsStore;
import com.techchallenge.waypointstats.messages.TopWaypointMsg;

/**
 * This class expose a REST API to call in order to get statistics
 * related to waypoints. 
 *
 */
@RestController
public class WaypointStatsController {	
	
	/**
	 * This function returns the top N waypoints associated with the
	 * most number of SIDs for all airports. It reads the response from
	 * a Kafka topic as String, then maps to a POJO and returns a json
	 * 
	 * @return TopWaypointMsg in JSON format or bad response if null
	 */
	@CrossOrigin(origins = "*")
	@GetMapping("/wpstats/sids/airport/topwaypoints")
	public ResponseEntity<TopWaypointMsg[]> topWaypointsSid() {
		TopWaypointMsg[] msgList;
		
		msgList = TopWaypointsStore.getInstance().getTopWaypointSid();
		
		if (msgList == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		return ResponseEntity.ok(msgList);
	}
	
	
	
	/**
	 * This function returns the top N waypoints associated with the
	 * most number of STARs for all airports. It reads the response from
	 * a Kafka topic as String, then maps to a POJO and returns a json
	 * 
	 * @return TopWaypointMsg in JSON format or bad response if null
	 */
	@CrossOrigin(origins = "*")
	@GetMapping("/wpstats/stars/airport/topwaypoints")
	public ResponseEntity<TopWaypointMsg[]> topWaypointsStar() {
		TopWaypointMsg[] msgList;
		
		msgList = TopWaypointsStore.getInstance().getTopWaypointStar();
		
		if (msgList == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		return ResponseEntity.ok(msgList);
	}
}


