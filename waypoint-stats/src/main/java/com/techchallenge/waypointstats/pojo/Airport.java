package com.techchallenge.waypointstats.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is a POJO class for an Airport object as per specification
 * of the JSON mapping returned from the OPEN ATMS API
 *
 */
public class Airport {
	private String uid;
	private String name;
	private String iata;
	private String icao;
	private double lat;
	private double lng;
	private int alt;
	
	public String getUid() {
		return uid;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIata() {
		return iata;
	}
	
	public String getIcao() {
		return icao;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public int getAlt() {
		return alt;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setIata(String iata) {
		this.iata = iata;
	}
	
	public void setIcao(String icao) {
		this.icao = icao;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public void setAlt(int alt) {
		this.alt = alt;
	}
	
	@Override
	public String toString() {
		String str;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			//Converting the Object to JSONString
			str = mapper.writeValueAsString(this);
		}
		catch(Exception error) {
			str = error.getMessage();
		}
		
		return str;
	}
}
