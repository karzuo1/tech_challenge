package com.techchallenge.waypointstats.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is a POJO class for a Waypoint object as per specification
 * of the JSON mapping returned from the OPEN ATMS API
 *
 */
public class Waypoint {
	private String uid;
	private String name;
	private double lat;
	private double lng;
	
	public String getUid() {
		return uid;
	}
	
	public String getName() {
		return name;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	@Override
	public String toString() {
		String str;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			//Converting the Object to JSONString
			str = mapper.writeValueAsString(this);
		}
		catch(Exception error) {
			str = error.getMessage();
		}
		
		return str;
	}
}

