package com.techchallenge.waypointstats.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class defines the message about which STARs are associated
 * with an airport
 * 
 */
public class AirportStar {
	private final Airport airport;
	private final STAR stars[];
	
	public AirportStar(@JsonProperty("airport") Airport airport, 
			@JsonProperty("stars")  STAR stars[]) {
		this.airport = airport;
		this.stars = stars;
	}
	
	public Airport getAirport() {
		return airport;
	}
	
	public STAR[] getStars() {
		return stars;
	}
}