package com.techchallenge.waypointstats.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class defines the message about which SIDs are associated
 * with an airport
 * 
 */
public class AirportSid {
	private final Airport airport;
	private final SID sids[];
	
	public AirportSid(@JsonProperty("airport") Airport airport, 
			@JsonProperty("sids")  SID sids[]) {
		this.airport = airport;
		this.sids = sids;
	}
	
	public Airport getAirport() {
		return airport;
	}
	
	public SID[] getSids() {
		return sids;
	}
}
