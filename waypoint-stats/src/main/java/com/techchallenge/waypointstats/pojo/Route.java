package com.techchallenge.waypointstats.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is an abstract POJO class for creating SID/STAR object as per specification
 * of the JSON mapping returned from the OPEN ATMS API
 *
 */
public abstract class Route {
	private String name;
	private Airport airport;
	private Waypoint waypoints[];
	
	public String getName() {
		return name;
	}
	
	public Airport getAirport() {
		return airport;
	}
	
	public Waypoint[] getWaypoints() {
		return waypoints;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAirport(Airport airport) {
		this.airport = airport;
	}
	
	public void setWaypoints(Waypoint waypoints[]) {
		this.waypoints = waypoints;
	}
	
	@Override
	public String toString() {
		String str;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			//Converting the Object to JSONString
			str = mapper.writeValueAsString(this);
		}
		catch(Exception error) {
			str = error.getMessage();
		}
		
		return str;
	}
}