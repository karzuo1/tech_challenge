package com.techchallenge.waypointstats.pojo;

/**
 * This is a POJO class for creating SID object as per specification
 * of the JSON mapping returned from the OPEN ATMS API
 *
 */
public class SID extends Route {
	//No changes
}
