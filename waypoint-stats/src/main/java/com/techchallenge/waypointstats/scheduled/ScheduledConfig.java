package com.techchallenge.waypointstats.scheduled;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.techchallenge.waypointstats.constants.IConfiguration;
import com.techchallenge.waypointstats.kafka.DataPublisher;

@Configuration
@EnableScheduling
public class ScheduledConfig {
	private DataPublisher sidStatProducer;
	private DataPublisher starStatProducer;
	
	public ScheduledConfig() {
		sidStatProducer = new DataPublisher("sidprod");
		starStatProducer = new DataPublisher("starprod");
	}

    @Scheduled(fixedRate = IConfiguration.SID_POLL_INTERVAL)
    public void executeTask1() {
    	sidStatProducer.produceSidStats();
    }
    
    @Scheduled(fixedRate = IConfiguration.STAR_POLL_INTERVAL)
    public void executeTask2() {
    	starStatProducer.produceStarStats();
    }
}