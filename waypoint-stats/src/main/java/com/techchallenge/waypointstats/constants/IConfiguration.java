package com.techchallenge.waypointstats.constants;

/**
 * General configurations for this module
 *
 */
public interface IConfiguration {
	//Scheduler configuration
	public static final int SID_POLL_INTERVAL = 60000;
	public static final int STAR_POLL_INTERVAL = 60000;
	
	//Open ATMs Interface
	public static final String KEY_STR = "api-key";
	public static final String API_KEY = //Need to safe keep this key
			"lgBaEkJ1TLQrwFDhtwe2mqLWIgoiyxue9kmrNkvOKpdjfhyXHIcdw7MNLmTLopH6";	
	public static final String BASE_URL = 
			"https://open-atms.airlab.aero/api/v1";
	
	//Module Configuration
	public static final int SID_TOP_N = 2;
	public static final int STAR_TOP_N = 2;
	
	//Kafka POll Settings (whether data changed)
	public static final int POLL_DELAY = 1000;
	public static final int SLEEP = 1000000;
}
