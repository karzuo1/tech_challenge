package com.techchallenge.waypointstats.constants;

public interface IKafkaConstants {
    public static final String KAFKA_BROKERS = "broker:29092";
    public static final Integer MESSAGE_COUNT=1000;
    public static final String GROUP_ID_CONFIG="consumerGroup1";
    public static final Integer MAX_NO_MESSAGE_FOUND_COUNT=100;
    public static final String OFFSET_RESET_LATEST="latest";
    public static final String OFFSET_RESET_EARLIER="earliest";
    public static final Integer MAX_POLL_RECORDS=1;
    
    //Kafka topics
    public static final String TOPIC_SID="sid-stats";
    public static final String TOPIC_STAR="star-stats";
    public static final String TOPIC_AIR_SID="airport-sids";
    public static final String TOPIC_AIR_STAR="airport-stars";
}
