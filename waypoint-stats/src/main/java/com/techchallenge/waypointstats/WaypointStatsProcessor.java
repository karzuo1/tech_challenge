package com.techchallenge.waypointstats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import com.techchallenge.waypointstats.constants.IConfiguration;
import com.techchallenge.waypointstats.messages.TopWaypoint;
import com.techchallenge.waypointstats.messages.TopWaypointMsg;
import com.techchallenge.waypointstats.pojo.AirportSid;
import com.techchallenge.waypointstats.pojo.AirportStar;
import com.techchallenge.waypointstats.pojo.Route;
import com.techchallenge.waypointstats.pojo.SID;
import com.techchallenge.waypointstats.pojo.STAR;
import com.techchallenge.waypointstats.pojo.Waypoint;


/**
 * This class processes and computes various statistics about waypoints 
 *
 */
public class WaypointStatsProcessor {
	
	/**
	 * This function returns the top N waypoints associated with the
	 * most number of SIDs for all airports
	 * 
	 * @return TopWaypointMsg in JSON format
	 */
	public TopWaypointMsg[] topWaypointsSid(AirportSid[] airSids) {
		ArrayList<TopWaypointMsg> msgList = new ArrayList<TopWaypointMsg>();
		TopWaypointMsg msg[]; //Array of messages for all airport

		//for each airport, compute the results
		for (int i=0; i<airSids.length; i++) {
			List<SID> sid; //List of SIDs for current airport
			TopWaypoint wp[]; //Top N Waypoints
			TopWaypointMsg currMsg; //message for current airport
			
			//Get the list of SIDs for current airport
			sid = Arrays.asList(airSids[i].getSids());

			//Compute top N waypoints
			wp = computeTopN(sid, IConfiguration.SID_TOP_N);
		
			//Create a message for this airport and store into list
			currMsg = new TopWaypointMsg(airSids[i].getAirport().getIcao(), wp);
			msgList.add(currMsg);
		}
		
		msg = msgList.toArray(new TopWaypointMsg[0]);
		return msg;
	}
	
	
	/**
	 * This function returns the top N waypoints associated with the
	 * most number of STARs for all airports
	 * 
	 * @return TopWaypointMsg in JSON format
	 */
	public TopWaypointMsg[] topWaypointsStar(AirportStar[] airStars) {
		ArrayList<TopWaypointMsg> msgList = new ArrayList<TopWaypointMsg>();
		TopWaypointMsg msg[]; //Array of messages for all airport
		
		//for each airport, compute the results
		for (int i=0; i<airStars.length; i++) {
			List<STAR> star; //List of STARs for current airport
			TopWaypoint wp[]; //Top N Waypoints
			TopWaypointMsg currMsg; //message for current airport
			
			//Get the list of SIDs for current airport
			star = Arrays.asList(airStars[i].getStars());
	
			//Compute top N waypoints
			wp = computeTopN(star, IConfiguration.STAR_TOP_N);
		
			//Create a message for this airport and store into list
			currMsg = new TopWaypointMsg(airStars[i].getAirport().getIcao(), wp);
			msgList.add(currMsg);
		}
		
		msg = msgList.toArray(new TopWaypointMsg[0]);
		return msg;
	}
	
	
	/**
	 * private function for computing the top N waypoints
	 * associated with the most SIDs/STARs in the given list.
	 * Array returned is sorted by frequency
	 * 
	 * @param List<Route>: list of SIDs or STARs
	 * @param n: the top n to compute
	 * @return TopWaypoint[]: The top n waypoints in a array
	 */
	private <T extends Route> TopWaypoint[] computeTopN(List<T> route, int n) {
		HashMap<String, Integer> freq = new HashMap<String, Integer>(); //assoc freq
		HashSet<Waypoint> routeWaypoints; //unit set of waypoints in route
		ArrayList<String> keys; //keys to freq hashmap
		String wpNames[]; //name of top n waypoints to be computed
		TopWaypoint twp[] = new TopWaypoint[0]; //top n waypoints msg
		
		//Iterates all SIDs/STARs and compute frequency of waypoints for airport
		Iterator<T> it = route.iterator();
		while(it.hasNext()) {
			Waypoint waypoints[];
			
			T currRoute = it.next();
			waypoints = currRoute.getWaypoints();
			
			//put waypoints in a set to get unique set
			routeWaypoints = new HashSet<Waypoint>(Arrays.asList(waypoints));
			waypoints = routeWaypoints.toArray(new Waypoint[0]);
			
			//Update the count
			for (int i=0; i<waypoints.length; i++) {
				freq.merge(waypoints[i].getName(), 1, Integer::sum);
			}
		}
		
		//Select top N
		keys = new ArrayList<String>(freq.keySet());
		wpNames = quickSelectN(keys, freq, 0, keys.size()-1, n);
		twp = new TopWaypoint[wpNames.length];
	
		//Sort top N by frequency (Descending)
		Collections.sort(Arrays.asList(wpNames), 
				 Comparator.comparing(s -> freq.get(s)).reversed());		
		
		//create msg for top waypoints
		for (int i=0; i<wpNames.length; i++) {
			twp[i] = new TopWaypoint(wpNames[i], freq.get(wpNames[i]));
		}
		
		return twp;
	}
	
	/**
	 * Select the top N waypoints with the highest frequency using modified
	 * quickselect algorithm. 
	 * 
	 * In this modified version, frequency greater than that of the pivot
	 * element is moved to left of pivot, and bigger or equal to the right.
	 * This partial sorting is repeated recursively until the order of pivot
	 * element equals that of N. We then return all elements from the left
	 * of pivot (inclusive of pivot) as the result.
	 * 
	 * In case where the number of elements is less than N, all elements are
	 * returned. This means that the given list can be empty (result will be 
	 * an empty list as well)
	 * 
	 * Note: Result is NOT sorted
	 * 
	 * @param arr (ArrayList<String>): a list of names of waypoints
	 * @param freq (HashMap<String,Integer>: Mapping of waypoint names to freq
	 * @param l: left index of array to search
	 * @param r: right index of array to search
	 * @param n: number of top waypoints to return
	 * @return An array of names top n waypoints, array is NOT sorted
	 */
	private String[] quickSelectN(ArrayList<String> arr, 
			HashMap<String, Integer>freq, int l, int r, int n) {
		
		//Base case where left index < n
		if (r < n) {
			return arr.subList(l, r+1).toArray(new String[0]);
		}
		
		//partition array based on a random pivot index
		int p; //pivot index
		int pfreq; //frequency of pivot chosen
		int left; //index of array from which freq <= freq of pivot
		Random rand = new Random();
		p = rand.nextInt(r-l+1) + l;
		pfreq = freq.get(arr.get(p));
		Collections.swap(arr, p, r); //shift pivot to end first
		
		left = l;
		for (int i=l; i<r; i++) {
			if (freq.get(arr.get(i)) > pfreq) {
				Collections.swap(arr, left, i); //move bigger freq to front
				left++;
			}
		}
		Collections.swap(arr, left, r);//move pivot to its place
		p = left; //update index of pivot
		
		if (p+1 == n) {
			return arr.subList(l, p+1).toArray(new String[0]);
		}
		
		if (n < p+1) { //top n elements in left partition
			return quickSelectN(arr, freq, l, p-1, n);
		}

		//top n elements include some from right partition
		String leftArr[] = arr.subList(l, p+1).toArray(new String[0]);
		String rightArr[] = quickSelectN(arr, freq, p+1, r, n);

		String combined[] = Stream.concat(Arrays.stream(leftArr), Arrays.stream(rightArr))
                .toArray(String[]::new);

		return combined;
	}
}
