import React, { Fragment } from 'react';

const List = (props) => {
  const route = props.route;
  const topWaypoints = props.topWaypoints;
  if (!topWaypoints || topWaypoints.length === 0) return <p>No data.</p>;

  //Custom headings
  var heading;
  if (route === "sid") {
    heading = "Top 2 Waypoints associated with most SIDs";
  } else if (route === "star") {
    heading = "Top 2 Waypoints associated with most STARs";
  }

  return (
    <ul>
      <h2 key={route+"h2"} className='list-head'>{heading}</h2>

      <table key={route}>
      <tbody>
        <tr className="index">
          <th>Airport ICAO Code</th>
          <th>Waypoint Name 1</th>
          <th>Association Count 1</th>
          <th>Waypoint Name 2</th>
          <th>Association Count 2</th>
        </tr>
        {topWaypoints.map((waypt) => {
          var resultCount = waypt.topWaypoints.length;
          var arr = [];

          for (var i=0; i<2; i++){
            if (i<resultCount){
              arr.push(waypt.topWaypoints[i]);
            }
            else if (i<2){
              arr.push({"name": "-----", "count": "---"});
            }
          }
          waypt.topWaypoints = arr;

          return (
            <tr>
              <th key={waypt.airport + route} className={route}>{waypt.airport}</th>
              {
                waypt.topWaypoints.map((wp) => {
                  return (
                    <Fragment>
                      <th key={wp.name + route + waypt.airport} className={route}>{wp.name}</th>
                      <th key={wp.count + route + waypt.airport} className={route}>{wp.count}</th>
                    </Fragment>
                  );
                })
              }
            </tr>
          );
        })}
        </tbody>
      </table>
    </ul>
  );
}

export default List;

