import React, { useEffect, useState } from 'react';
import './App.css';
import List from './components/List';
import withListLoading from './components/withListLoading';
function App() {
  const ListLoading = withListLoading(List);
  const [appState, setAppState] = useState({
    sidLoading: false,
    sid: null,
    starLoading: false,
    star: null
  });

  function loadData() {
    setAppState({ sidLoading: true, starLoading: true });
    const sidUrl = "http://34.126.130.216:8090/wpstats/sids/airport/topwaypoints";
    const starUrl = "http://34.126.130.216:8090/wpstats/stars/airport/topwaypoints";

    Promise.all([
      fetch(sidUrl),
      fetch(starUrl),
    ]).then(allResponses => {
      var sidResponse;
      var starResponse;

      allResponses[0].json().then((sid) => {
        sidResponse = sid;
        allResponses[1].json().then((star) => {
          starResponse = star;
          setAppState({ starLoading: false, star: starResponse, sidLoading: false, sid: sidResponse });
        });
      });
    });
  };

  useEffect(() => {
    loadData();
    const id = setInterval(() => {
      loadData();
    }, 5000);

    return () => clearInterval(id);
  }, [setAppState]);

  return (
    <div className='App'>
      <div className='container'>
        <h1>Air Traffic Control Waypoint Statistics</h1>
      </div>
      <div className='sid-container'>
        <ListLoading isLoading={appState.sidLoading} route="sid" topWaypoints={appState.sid} />
      </div>
      <div className='star-container'>
        <ListLoading isLoading={appState.starLoading} route="star" topWaypoints={appState.star} />
      </div>
      <footer>
        <div className='footer'>
          Built by Alvin Lee
        </div>
      </footer>
    </div>
  );
}
export default App;
